# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.8.2] - 2023-03-02
### Fixed
- Fix HTTP header injection in Spring Message Converter for `MultiPartMessage`s. As the HTTP headers should be set before the output stream is written to.

## [4.8.1] - 2023-02-20
### Changed
- Allowing async responses without specifically setting `Response-Type` header. The `Response-Type` header is now only used to indicate that when a message contains a `correlationMessage` identifier it must be handled in an async way.

## [4.8.0] - 2022-10-04
### Added
- Updated documentation

### Changed
- Dependency updates:
  - Spring Boot 2.4.2 -> 2.7.4
  - Jackson 2.13.1 -> 2.13.4
  - JSON 20210307 -> 20220320
  - Guava 31.0.1-jre -> 31.1-jre
  - Wiremock 2.32.0 -> 2.34.0

## [4.7.0] - 2022-08-23
### Added
- Maven snapshot and release separation, snapshots are published when not in the `master` branch and releases are published when in the `master` branch

### Changed
- Broker interaction pattern changed to core container API instead of substitution inside the camel route
- Removed deprecated BrokerClient methods