# Base Data App
The Base Data App is used to simplify IDS development by providing a generic interface and handling of messages forwarded from the Trusted core container.
A compiled version of the JAR is available in the [TSG Maven Repository](https://nexus.dataspac.es/#browse/browse:tsg-maven:nl%2Ftno%2Fids%2Fbase-data-app).

The `IdsRestController` class is the main class responsible for handling incoming IDS messages on the `/router` endpoint.

The `IdsRestController` class is the main class of the Base Data App which can be extended by Data App implementation. It contains a Spring REST Controller with a `/router` endpoint on which messages arrive. A `MessageHandler` interface is provided so that own handlers for messages can be created, the message is routed based on the header message type. Therefore, message handlers should implement a distinct subset of header message types, as the first matching message handler is used for the handling.

## Write your own Data App
The following steps should be taken to write your own data app:

### Include the dependency in your project
```gradle
dependencies {
  // ...
  compile group: 'nl.tno.ids', name: 'base-data-app', version: '4.8.0'
  // ...
}

repositories {
  // ...
  maven {
    url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
  }
  maven {
    url = uri("https://nexus.dataspac.es/repository/tsg-maven/")
  }
  // ...
}
```

### Provide a Spring Boot starting point
Your Spring Boot Data App should be configured so that the components in the Base Data App library are also processed by Spring Boot.
To do that, write you main starting class like so: (note that this example is Kotlin, but Java differs only slightly)
```kotlin
package nl.tno.ids.example

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["nl.tno.ids", "YOUR_APPLICATION_NAMESPACE"])
@ConfigurationPropertiesScan(basePackages = ["nl.tno.ids", "YOUR_APPLICATION_NAMESPACE"])
class Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
```

- `@SpringBootApplication(scanBasePackages = ["nl.tno.ids", "YOUR_APPLICATION_NAMESPACE"])` assures that the classes present in Base Data App are injected in your own Data App. Replace `YOUR_APPLICATION_NAMESPACE` with the namespace of your application, so that it will be picked up in the Spring Context.
- `@ConfigurationPropertiesScan(basePackages = ["nl.tno.ids", "YOUR_APPLICATION_NAMESPACE"])` assures that configuration is picked up correctly. Replace `YOUR_APPLICATION_NAMESPACE` with the namespace of your application, so that it will be picked up in the Spring Context.


### Provide Configuration
Default Spring Boot Configuration is used to configure the application (additional information regarding Spring boot configuration can be found here: https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config).
The following properties are mandatory for the data app to work
```yaml
core-container:
  https-forward-endpoint: "http://core-container:8080/https_out"
  apiEndpoint: "http://localhost:8082/"
  apiKey: "APIKEY-ABCDEFG"
ids:
  connector-id: "id"
  participant-id: "participant"
```

#### Helm deployment
The provided [Helm Chart](https://gitlab.com/tno-tsg/helm-charts/connector) helps to deploy necessary resources to Kubernetes.
When using the Helm Chart, the `ids` and `core-container` sections are configured automatically.

### Write your own IDS Message Handlers
IDS messages are handled by writing a class that inherits from `nl.tno.ids.base.MessageHandler`.
The `MessageHandler` interface has a `handle` function that should be overridden.
The Base Data App assures that the `handle` function is called with the right parameters.

```kotlin
package nl.tno.ids.example
import de.fraunhofer.iais.eis.QueryMessage
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.base.configuration.IdsConfig
import org.springframework.http.ResponseEntity
import java.util.logging.Logger
import org.slf4j.LoggerFactory

// Inherit from MessageHandler to register message handlers of a specific IDS message.
// Spring will register the Message Handler using Dependency Injection
class QueryMessageHandler(private val config: IdsConfig) : MessageHandler<QueryMessage> {
  override fun handle(header: QueryMessage, p1: String?): ResponseEntity<*> {
    val logger = LoggerFactory.getLogger(QueryMessageHandler::class.java)
    logger.info("Received query message: {}, {}", header, p1)
    return ResponseEntity.ok(config.connectorId)
  }
}
```