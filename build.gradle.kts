import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.7.20"
  kotlin("plugin.spring") version "1.7.20"
  id("io.spring.dependency-management") version "1.0.14.RELEASE"
  `maven-publish`
  `java-library`
}

group = "nl.tno.ids"
version = "4.8.2"
java.sourceCompatibility = JavaVersion.VERSION_11

val springVersion = "2.7.4"
val jacksonVersion = "2.13.4"
val jsonVersion = "20220320"
val idsVersion = "4.0.2-SNAPSHOT"
val idsInformationModelVersion = "4.2.7"
val idsInformationModelSerializerVersion = "4.2.8"
val guavaVersion = "31.1-jre"
val wiremockVersion = "2.34.0"

dependencies {
  api("org.springframework.boot:spring-boot-starter-web:$springVersion")
  api("com.fasterxml.jackson.module:jackson-module-kotlin:$jacksonVersion")
  api("org.jetbrains.kotlin:kotlin-reflect")
  api("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

  // IDS dependencies
  api("nl.tno.ids.common:ids-information-model:$idsVersion") {
    exclude("de.fraunhofer.iais.eis.ids.infomodel:java")
    exclude("de.fraunhofer.iais.eis.ids:infomodel-serializer")
  }
  api("de.fraunhofer.iais.eis.ids.infomodel:java:$idsInformationModelVersion")
  api("de.fraunhofer.iais.eis.ids:infomodel-serializer:$idsInformationModelSerializerVersion")

  implementation("org.json:json:$jsonVersion")
  implementation("com.google.guava:guava:$guavaVersion")

  // Test dependencies
  testApi("org.springframework.boot:spring-boot-starter-test:$springVersion")
  testImplementation("com.github.tomakehurst:wiremock-jre8:$wiremockVersion")
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "11"
  }
}

java {
  withSourcesJar()
}

repositories {
  mavenCentral()
  maven {
    url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
  }
  maven {
    url = uri("https://nexus.dataspac.es/repository/tsg-maven/")
  }
}

/**
 * Publish settings for publishing in a Maven repository
 */
publishing {
  publications {
    if (System.getenv().containsKey("CI")) {
      create<MavenPublication>("maven") {
        from(components["java"])
        groupId = project.group.toString()
        artifactId = project.name
        version = project.version.toString()
        pom {
          description.set(project.description)
        }
      }
    }
    create<MavenPublication>("mavenSnapshot") {
      from(components["java"])
      groupId = project.group.toString()
      artifactId = project.name
      version = "${project.version}-SNAPSHOT"
      pom {
        description.set(project.description)
      }
    }
  }
  repositories {
    maven {
      name = "tsg"
      url = uri("https://nexus.dataspac.es/repository/tsg-maven/")
      credentials {
        username = System.getenv("TSG_NEXUS_USERNAME")
        password = System.getenv("TSG_NEXUS_PASSWORD")
      }
    }
    if (!System.getenv().containsKey("CI")) {
      mavenLocal()
    }
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}
