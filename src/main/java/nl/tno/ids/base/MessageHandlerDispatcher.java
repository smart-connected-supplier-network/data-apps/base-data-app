package nl.tno.ids.base;

import de.fraunhofer.iais.eis.Message;
import nl.tno.ids.base.multipart.MultiPartMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Message Handler Dispatcher that directs IDS messages to the corresponding MessageHandler for that type of IDS
 * message.
 * Due to the requirement of this functionality of both covariant as contravariant generics, this class is written
 * in Java instead of Kotlin.
 */
@Component
@SuppressWarnings("rawtypes")
public class MessageHandlerDispatcher {
    protected static final Logger LOG = LoggerFactory.getLogger(MessageHandlerDispatcher.class);
    /** List of registered message handler to use for dispatching */
    private final List<MessageHandler> messageHandlers;

    /** Public constructor receiving the message handler list */
    public MessageHandlerDispatcher(List<MessageHandler> messageHandlers) {
        this.messageHandlers = messageHandlers;
    }

    /**
     * Handle an incoming MultiPartMessage and route it to the corresponding message handler that accepts it
     * @param multiPartMessage Incoming MultiPartMessage
     * @return Optional ResponseEntity if a suitable message handler is found and the message is handled
     * @param <I> IDS Message class
     */
    @SuppressWarnings("unchecked")
    public <I extends Message> Optional<ResponseEntity> handleMessage(MultiPartMessage<I> multiPartMessage) {
        for (MessageHandler messageHandler : messageHandlers) {
            try {
                return Optional.of(messageHandler.handle(multiPartMessage));
            } catch (ClassCastException e) {
                LOG.trace("{} could not handle {}", messageHandler.getClass().getCanonicalName(), multiPartMessage.getHeader().getClass().getCanonicalName());
            } catch (IOException e) {
                LOG.error("IO Exception while handling message:", e);
            }
        }
        LOG.error("No handler for operation found!");
        return Optional.empty();
    }
}
