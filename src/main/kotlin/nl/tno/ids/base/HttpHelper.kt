package nl.tno.ids.base

import de.fraunhofer.iais.eis.Described
import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.ModelClass
import nl.tno.ids.base.configuration.CoreContainerConfig
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.base.infomodel.toJsonLD
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.ContentType
import org.apache.http.entity.InputStreamEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.File
import java.io.InputStream
import java.util.*

/**
 * Http Helper component for interacting with the HTTPS & IDSCP egress endpoints of the Core Container
 */
@Component
class HttpHelper(private val coreContainerConfig: CoreContainerConfig, private val httpClient: CloseableHttpClient) {
    private val log = LoggerFactory.getLogger(HttpHelper::class.java)

    /**
     * Sends an IDS HTTP multipart message to another connector with the given payload string
     */
    fun toHTTP(
        receiver: String,
        header: Message,
        payload: String?,
        contentType: ContentType = ContentType.APPLICATION_JSON
    ): Pair<Int, MultiPartMessage<*>> {
        return forwardSynchronous(coreContainerConfig.httpsForwardEndpoint, receiver, header, payload, null, contentType)
    }

    /**
     * Sends an IDS HTTP multipart message to another connector with the given inputStream as payload body
     */
    fun toHTTP(
        receiver: String,
        header: Message,
        inputStream: InputStream,
        contentType: ContentType = ContentType.APPLICATION_JSON
    ): Pair<Int, MultiPartMessage<*>> {
        return forwardSynchronous(coreContainerConfig.httpsForwardEndpoint, receiver, header, null, inputStream, contentType)
    }

    /**
     * Sends an IDS HTTP multipart message to another connector with the given described content body
     */
    fun toHTTP(receiver: String, header: Message, payload: ModelClass): Pair<Int, MultiPartMessage<*>> {
        return forwardSynchronous(
            coreContainerConfig.httpsForwardEndpoint, receiver, header, payload.toJsonLD(), null,
            ContentType.APPLICATION_JSON
        )
    }

    /**
     * Sends an IDS HTTP multipart message to another connector
     */
    fun toHTTP(receiver: String, multiPartMessage: MultiPartMessage<*>): Pair<Int, MultiPartMessage<*>> {
        return forwardSynchronousMultipart(
            coreContainerConfig.httpsForwardEndpoint, receiver, multiPartMessage
        )
    }

    /**
     * Sends an IDSCP message to another connector with the given payload string
     */
    fun toIDSCP(receiver: String, header: Message, payload: String?): Int {
        return forwardAsynchronous(
            coreContainerConfig.idscpForwardEndpoint,
            receiver,
            header,
            payload,
            null,
            ContentType.APPLICATION_JSON
        )
    }

    /**
     * Sends an IDSCP message to another connector given inputStream as payload body
     */
    fun toIDSCP(receiver: String, header: Message, inputStream: InputStream): Int {
        return forwardAsynchronous(
            coreContainerConfig.idscpForwardEndpoint,
            receiver,
            header,
            null,
            inputStream,
            ContentType.APPLICATION_JSON
        )
    }

    /**
     * Sends an IDSCP message to another connector with the given described content body
     */
    fun toIDSCP(receiver: String, header: Message, payload: Described): Int {
        return forwardAsynchronous(
            coreContainerConfig.idscpForwardEndpoint, receiver, header, payload.toJsonLD(), null,
            ContentType.APPLICATION_JSON
        )
    }

    /**
     * Sends an IDSCP message to another connector
     */
    fun toIDSCP(receiver: String, multiPartMessage: MultiPartMessage<*>): Int {
        return forwardAsynchronousMultipart(
            coreContainerConfig.idscpForwardEndpoint, receiver, multiPartMessage
        )
    }

    /**
     * Forward a request asynchronously as multi part message
     */
    fun forwardAsynchronous(
        address: String?,
        receiver: String?,
        header: Message,
        payload: String?,
        inputStream: InputStream?,
        contentType: ContentType
    ): Int {
        log.info("Forwarding Asynchronous: {}, {}, {}", address, receiver, header.javaClass.simpleName)
        val multiPartMessage = inputStream?.let { MultiPartMessage(header, it) } ?: MultiPartMessage(header, payload)
        multiPartMessage.httpHeaders["Content-Type"] = contentType.mimeType

        return forwardAsynchronousMultipart(address, receiver, multiPartMessage)
    }

    /**
     * Forward a request asynchronously as multi part message
     */
    fun forwardAsynchronousMultipart(
        address: String?,
        receiver: String?,
        multiPartMessage: MultiPartMessage<*>
    ): Int {
        val httpPost = HttpPost(address)
        httpPost.addHeader("Forward-To", receiver)
        coreContainerConfig.apiKey?.let {
            httpPost.addHeader("Authorization", "Bearer $it")
        }

        val file = File.createTempFile("base-data-app-", ".http")
        multiPartMessage.writeToOutputStream(file.outputStream(), true)
        multiPartMessage.httpHeaders.forEach { (key, value) ->
            httpPost.addHeader(key, value)
        }

        httpPost.entity = InputStreamEntity(file.inputStream())
        try {
            return httpClient.execute(httpPost).use { response ->
                val statusCode = response.statusLine.statusCode
                if (statusCode > 300) {
                    Scanner(response.entity.content).use { scanner ->
                        val body = scanner.useDelimiter("\\A").next()
                        log.error("Non-OK result: {} {}", statusCode, body)
                    }
                }
                statusCode
            }
        } finally {
            file.delete()
        }
    }

    /**
     * Forward a request synchronously as multi part message
     */
    fun forwardSynchronous(
        address: String,
        receiver: String,
        header: Message,
        payload: String?,
        inputStream: InputStream?,
        contentType: ContentType
    ): Pair<Int, MultiPartMessage<*>> {
        log.info("Forwarding Synchronous: {}, {}, {}", address, receiver, header.javaClass.simpleName)

        val multiPartMessage = inputStream?.let { MultiPartMessage(header, it) } ?: MultiPartMessage(header, payload)
        multiPartMessage.httpHeaders["Content-Type"] = contentType.mimeType

        return forwardSynchronousMultipart(address, receiver, multiPartMessage)
    }

    /**
     * Forward a MutliPartMessage synchronously
     */
    fun forwardSynchronousMultipart(
        address: String,
        receiver: String,
        multiPartMessage: MultiPartMessage<*>
    ): Pair<Int, MultiPartMessage<*>> {
        val httpPost = HttpPost(address)
        if (receiver.isNotEmpty()) {
            httpPost.addHeader("Forward-To", receiver)
        }
        coreContainerConfig.apiKey?.let {
            httpPost.addHeader("Authorization", "Bearer $it")
        }
        val file = File.createTempFile("base-data-app-", ".http")
        multiPartMessage.writeToOutputStream(file.outputStream(), true)
        multiPartMessage.httpHeaders.forEach { (key, value) ->
            httpPost.addHeader(key, value)
        }

        httpPost.entity = InputStreamEntity(file.inputStream())
        try {
            return httpClient.execute(httpPost).use { response ->
                val statusCode = response.statusLine.statusCode
                if (statusCode !in 200..299) {
                    log.info("Received response with statuscode: {}", statusCode)
                }
                val contentTypeHeader = response.getFirstHeader("Content-Type")
                val contentTypeValue = contentTypeHeader?.value
                val responseMultiPartMessage = MultiPartMessage.parse(response.entity.content, contentTypeValue)

                response.allHeaders.forEach { header ->
                    responseMultiPartMessage.httpHeaders[header.name] = header.value
                }
                Pair(statusCode, responseMultiPartMessage)
            }
        } finally {
            file.delete()
        }
    }
}