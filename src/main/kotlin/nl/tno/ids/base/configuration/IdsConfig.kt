package nl.tno.ids.base.configuration

import nl.tno.ids.base.FederationType
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

/**
 * IDS Data App configuration
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "ids")
data class IdsConfig(
  /** IDS connector ID */
  val connectorId: String,
  /** IDS participant ID */
  val participantId: String,
  /** IDS information model version */
  val modelVersion: String = "4.1.1",
  /** Data App ID */
  val appId: String = "data-app",
  /** Cache invalidation in milliseconds */
  val cacheInvalidationPeriod: Long = 30*60*1000L,
  /** Validate resource state at Core Container */
  val validateResources: Boolean = true,
  /** Validate resource state at Core Container */
  val validateResourcesInterval: Long = 30000L,
  /** Retrieve list of federated brokers from the configured broker */
  val retrieveFederatedBrokers: Boolean = false,
  /** Default federation type */
  val defaultFederationType: FederationType = FederationType.ON_EMPTY_RESULT,
  /** Federation tag scope */
  val federationTagScope: List<String> = emptyList()
)
