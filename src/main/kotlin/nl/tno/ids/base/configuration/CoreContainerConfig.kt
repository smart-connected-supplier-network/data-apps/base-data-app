package nl.tno.ids.base.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

/**
 * Configuration for communication with the Core Container
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "core-container")
data class CoreContainerConfig(
  /** HTTPS Forward endpoint used to send messages to other connectors via HTTP */
  val httpsForwardEndpoint: String,
  /** IDSCP Forward endpoint used to send messages to other connectors via IDSCP */
  val idscpForwardEndpoint: String?,
  /** API Endpoint used for interacting with the management API of the Core Container */
  val apiEndpoint: String?,
  /** API Key used for communication with the Core Container */
  val apiKey: String? = null
)