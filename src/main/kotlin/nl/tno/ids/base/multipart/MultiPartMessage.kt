package nl.tno.ids.base.multipart


import de.fraunhofer.iais.eis.Message
import nl.tno.ids.common.serialization.SerializationHelper
import nl.tno.ids.base.infomodel.toJsonLD
import org.apache.commons.lang3.RandomStringUtils
import org.slf4j.LoggerFactory
import java.io.*

/**
 * Exception class to indicate errors in multipart formatted messages
 */
class MultiPartParseException(message: String?) : Exception(message)

/**
 * IDS MIME Multipart Message container for (de)serializing HTTP MIME Multipart requests/responses
 */
class MultiPartMessage<I : Message>(
    /**
     * IDS Header Message
     */
    val header: I,
    /**
     * Message payload
     */
    val payload: Part? = null,
    /**
     * Envelope HTTP headers
     */
    val httpHeaders: MutableMap<String, String> = mutableMapOf()
) {
    companion object {
        /**
         * Regex pattern for finding the boundary in the content-type
         */
        private val boundaryPattern = "(.*?)boundary=(.*);.*".toRegex()

        /**
         * Regex pattern for finding the name of a part
         */
        private val namePattern = ".*name=\"(.*)\"".toRegex()
        private val LOG = LoggerFactory.getLogger(MultiPartMessage::class.java)

        /**
         * Parse the given String and optional Content-Type to a MultiPartMessage
         */
        fun parse(s: String, contentType: String? = null): MultiPartMessage<Message> {
            return parse(null, s, contentType)
        }

        /**
         * Parse the given InputStream and optional Content-Type to a MultiPartMessage
         */
        fun parse(inputStream: InputStream, contentType: String? = null): MultiPartMessage<Message> {
            return parse(inputStream, null, contentType)
        }

        /**
         * Parse the given InputStream line by line and build up the MultiPartMessage model
         *
         * Process of reading Multipart is:
         * - Read main HTTP headers if the inputstream is a full HTTP message and not just the body
         * - Detect the start of a line denoted by --$boundary
         * - Read part HTTP headers until an empty line is found
         * - Copy the body to the inputstream of the Part
         */
        private fun parse(inputStream: InputStream?, s: String?, contentType: String? = null, mainHttpHeaders: Boolean = false): MultiPartMessage<Message> {
            var boundary = contentType?.let {
                boundaryPattern.find(it)?.groupValues?.get(2)
            }
            val httpHeaders: MutableMap<String,String> = mutableMapOf()
            var readingMainHttpHeaders = mainHttpHeaders
            var readingPartHttpHeaders = false
            val namedParts: MutableMap<String, Part> = mutableMapOf()
            var part = Part()
            var firstLine = true
            var linesRead = 0
            val parseLine: (String) -> Unit = { line ->
                linesRead++
                if (boundary == null && line.startsWith("--")) {
                    boundary = line.substring(2)
                    LOG.debug("Found boundary (by estimation): $boundary")
                }

                when {
                    readingMainHttpHeaders -> {
                        if (line.startsWith("--$boundary")) {
                            readingMainHttpHeaders = false
                        } else {
                            line.split(':').let {
                                httpHeaders.put(it.first().trim(), it.last().trim())
                            }
                        }
                    }
                    line.startsWith("--$boundary") ->  {
                        namedParts[part.name] = part
                        part = Part()
                        readingPartHttpHeaders = true
                        firstLine = true
                    }
                    readingPartHttpHeaders -> {
                        if (line.contains(':')) {
                            line.split(':').let {
                                if (it.first().lowercase() == "content-disposition") {
                                    part.name = namePattern.find(it.last())?.groupValues?.get(1) ?: throw MultiPartParseException("No name parameter for Content-Disposition header")
                                }
                                part.httpHeaders[it.first().trim()] = it.last().trim()
                            }
                        } else {
                            readingPartHttpHeaders = false
                        }
                    }
                    else -> {
                        if (!firstLine) {
                            part.write("\n")
                        }
                        part.write(line)
                        firstLine = false
                    }
                }
            }
            inputStream?.bufferedReader()?.forEachLine { line -> parseLine(line) }
            s?.lineSequence()?.forEach { line -> parseLine(line) }

            val header = namedParts.remove("header")?.let { headerPart ->
                try {
                    SerializationHelper.getInstance()
                        .fromJsonLD(String(headerPart.inputStream().readAllBytes()), Message::class.java)
                } catch (e: Exception) {
                    LOG.warn("Error parsing header JSON-LD")
                    LOG.debug(String(headerPart.inputStream().readAllBytes()))
                    throw e
                }
            } ?: throw MultiPartParseException("Multipart did not contain a header part (${namedParts.keys} in $linesRead lines)\n${inputStream?.let { it.reset(); it.readAllBytes().decodeToString() }}")
            val payloadPart = namedParts.remove("payload")
            namedParts.values.forEach { it.delete() }
            return MultiPartMessage(header, payloadPart, httpHeaders)
        }
    }

    private val boundary: String = RandomStringUtils.randomAlphanumeric(30)

    fun contentType() = "multipart/form-data; boundary=$boundary; charset=UTF-8"

    /**
     * Create MultiPartMessage container from header and payload separately
     */
    constructor(header: I, payload: String? = null, httpHeaders: MutableMap<String, String> = mutableMapOf(), contentType: String? = null)
            : this(header, payload?.let { Part(it, contentType) }, httpHeaders)
    /**
     * Create MultiPartMessage container from header and payload separately
     */
    constructor(header: I, payload: InputStream, httpHeaders: MutableMap<String, String> = mutableMapOf(), contentType: String? = null)
            : this(header, Part(payload, contentType), httpHeaders)

    /**
     * Write the MultiPartMessage to an HTTP MIME compliant body String
     */
    override fun toString(): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        writeToOutputStream(byteArrayOutputStream)
        return String(byteArrayOutputStream.toByteArray())
    }

    /**
     * Write the MultiPartMessage to an HTTP MIME compliant body OutputStream
     *
     * Optionally removes temporarily created files in case of large (>10MB) parts
     */
    fun writeToOutputStream(outputStream: OutputStream, deleteFile: Boolean = false) {
        outputStream.bufferedWriter(Charsets.UTF_8).apply {
            httpHeaders.filterKeys { it.lowercase() == "content-type" }.entries.firstOrNull()?.let {
                httpHeaders.remove(it.key)
            }
            httpHeaders["Content-Type"] = "multipart/form-data; boundary=$boundary; charset=UTF-8"
            write("--$boundary\r\n")
            val headerJsonLD = header.toJsonLD().trim()
            write("Content-Disposition: form-data; name=\"header\"\r\n")
            write("Content-Type: application/ld+json\r\n")
            write("\r\n")
            write("$headerJsonLD\r\n")
            if (payload != null) {
                write("--$boundary\r\n")
                write("Content-Disposition: form-data; name=\"payload\"\r\n")
                payload.httpHeaders.filterKeys { it != "Content-Disposition"}.forEach { (key, value) ->
                    write("$key: $value\r\n")
                }
                write("\r\n")
                flush()
                if (payload.inputStream().markSupported()) {
                    payload.inputStream().reset()
                }
                payload.inputStream().transferTo(outputStream)
                if (deleteFile) {
                    payload.delete()
                }
                write("\r\n")
            }
            write("--$boundary--")
            flush()
        }
    }

}

/**
 * Parsed Part of the Multipart message
 */
class Part() {
    /**
     * Part name
     */
    var name: String = "unknown"

    /**
     * Body OutputStream
     */
    private var outputStream: OutputStream = ByteArrayOutputStream()

    /**
     * Part HTTP headers
     */
    val httpHeaders: MutableMap<String, String> = mutableMapOf()

    /**
     * Size of body (in bytes)
     */
    var size = 0

    /**
     * Backing file for the body (when the body becomes too large for memory processing)
     */
    private var file: File? = null

    companion object {
        private val LOG = LoggerFactory.getLogger(Part::class.java)
    }

    /**
     * Constructor based on payload and content type
     */
    constructor(payload: String, contentType: String?): this() {
        write(payload)
        contentType?.let {
            httpHeaders["Content-Type"] = contentType
        }
    }

    /**
     * Constructor based on payload inputstream and content type
     */
    constructor(payload: InputStream, contentType: String?): this() {
        write(payload)
        contentType?.let {
            httpHeaders["Content-Type"] = contentType
        }
    }

    /**
     * Get the Content-Type of the Part, if specified
     */
    fun getContentType(): String? = httpHeaders["Content-Type"]

    /**
     * Write a string to the body of the Part
     * Will create a new file after 10MB
     */
    fun write(s: String) {
        s.toByteArray(Charsets.UTF_8).let {
            if (file == null && size + s.length > 10485760) {
                createFile()
            }
            outputStream.write(it)
            size += it.size
        }
    }

    /**
     * Write a string to the body of the Part
     * Will create a new file after 10MB
     */
    fun write(inputStream: InputStream) {
        val buffer = ByteArray(10240)
        inputStream.use { input ->
            while (true) {
                val length = input.read(buffer)
                if (length <= 0)
                    break
                if (file == null && size + length > 10485760) {
                    createFile()
                }
                outputStream.write(buffer, 0, length)
                size += size
            }
        }
    }

    /**
     * Get the InputStream of the Part
     */
    fun inputStream(): InputStream {
        return file?.inputStream() ?: (outputStream as ByteArrayOutputStream).toByteArray().inputStream()
    }

    /**
     * Get ByteArray of the Part (will load the full body in memory)
     */
    fun asByteArray(): ByteArray {
        return inputStream().readAllBytes()
    }

    /**
     * Get String of the Part (will load the full body in memory)
     */
    fun asString(): String {
        return inputStream().readAllBytes().toString(Charsets.UTF_8)
    }

    /**
     * Create a temporary file for storing large bodies
     */
    private fun createFile() {
        file = File.createTempFile("multipart", ".tmp")
        LOG.debug("Created tmp multipart file: $file")
        (outputStream as ByteArrayOutputStream).writeTo(file!!.outputStream())
        outputStream = FileOutputStream(file!!, true)
    }

    /**
     * Delete the temporary file if it exists
     */
    fun delete() {
        file?.delete()
    }
}