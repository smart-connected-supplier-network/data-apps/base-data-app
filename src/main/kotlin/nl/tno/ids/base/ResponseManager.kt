package nl.tno.ids.base

import de.fraunhofer.iais.eis.Message
import org.springframework.stereotype.Component
import java.io.InputStream
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap

/**
 * Spring component for keeping track of response handlers that can be completed upon a response.
 */
@Component
class ResponseManager {
    /** In-flight asynchronous requests that did not receive a response yet */
    private val responses: ConcurrentHashMap<String, CompletableFuture<Pair<Message, InputStream?>>> = ConcurrentHashMap()

    /** Remove response handler that either is successfully handled or failed */
    fun removeResponseHandler(messageId: String): CompletableFuture<Pair<Message, InputStream?>>? {
        if (responses.containsKey(messageId)) {
            return responses.remove(messageId)
        }
        return null
    }

    /** Register response handler for handling the response of an outgoing asynchronous request */
    fun registerResponseHandler(correlationId: String, handler: CompletableFuture<Pair<Message, InputStream?>>) {
        responses[correlationId] = handler
    }
}