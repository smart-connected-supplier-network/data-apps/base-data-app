package nl.tno.ids.base

import de.fraunhofer.iais.eis.Resource
import de.fraunhofer.iais.eis.ResourceCatalog
import de.fraunhofer.iais.eis.ResourceCatalogBuilder
import nl.tno.ids.base.configuration.CoreContainerConfig
import nl.tno.ids.base.configuration.IdsConfig
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.net.URI
import java.util.concurrent.ConcurrentHashMap
import kotlin.system.exitProcess

/**
 * Resource Publisher component for convenient resource state management between the Core Container and this Data App
 */
@Component
@EnableScheduling
class ResourcePublisher(
  /** Core Container configuration used for validating whether the API endpoint is configured */
  private val coreContainerConfig: CoreContainerConfig,
  /** IDS Data App configuration */
  private val idsConfig: IdsConfig,
  /** Core Container API for interaction with the Core Container Management API */
  private val coreContainerAPI: CoreContainerAPI
) {
  private val log = LoggerFactory.getLogger(ResourcePublisher::class.java)
  /** Local cache of resources for detecting changes between the state of the Data App and Core Container */
  val resourceCache: MutableMap<URI, Resource> = ConcurrentHashMap()
  /** Catalog ID used for this Data App */
  private val catalogId: String = CoreContainerAPI.uriEncode("${idsConfig.connectorId}${if (idsConfig.connectorId.startsWith("urn")) ":" else "/"}${idsConfig.appId}")

  /** Scheduled task to validate the state between the Data App and Core Container */
  @Scheduled(
    initialDelayString = "\${ids.validateResourcesInterval:30000}",
    fixedDelayString = "\${ids.validateResourcesInterval:30000}")
  fun validateResourceState() {
    if (idsConfig.validateResources && resourceCache.isNotEmpty()) {
      val remoteResources = getResources()?.offeredResource
        ?.map {
          it.id.toString()
        }?.toSet() ?: emptySet()
      val localResources = resourceCache.keys.map { it.toString() }.toSet()
      if (remoteResources != localResources) {
        log.info("Inconsistency detected in local resources and resources available at the core container, replacing all resources at the broker with the local ones")
        log.debug("Remote: $remoteResources")
        log.debug("Local: $localResources")
        publishResourcesToCoreContainer(resourceCache.values.toList())
      } else {
        log.debug("Local and remote resources are consistent")
      }
    }
  }

  /**
   * Get the Resource catalog the Core Container has present for this data app
   */
  fun getResources(): ResourceCatalog? {
    if (coreContainerConfig.apiEndpoint.isNullOrBlank()) {
      return null
    }

    return try {
      coreContainerAPI
        .get("resources/$catalogId", ResourceCatalog::class.java)
    } catch (e: CoreContainerApiException) {
      log.warn(e.message)
      null
    }
  }

  /**
   * Let the Core container known there is a new resource available in the Data App.
   * The Core Container will republish its self-description to the broker.
   */
  fun publishResourceToCoreContainer(resource: Resource, retries: Int = 3, retryInterval: Long = 10000L) {
    if (coreContainerConfig.apiEndpoint.isNullOrBlank()) {
      throw RuntimeException("No Core Container API endpoint set")
    }

    try {
      coreContainerAPI.post("resources/$catalogId", resource, String::class.java)
      log.info("Published DataResource to core-container")
      resourceCache[resource.id] = resource
      return
    } catch (e: CoreContainerApiException) {
      log.error("Failed to post resource to core-container, HTTP: {}", e.message)
    }

    if (retries <= 1) {
      log.error("No retries left, stopping container")
      exitProcess(1)
    } else {
      log.error("Retrying")
      Thread.sleep(retryInterval)
      publishResourceToCoreContainer(resource, retries - 1, retryInterval)
    }
  }

  /**
   * Let the Core container known there are new resources available in the Data App.
   * The Core Container will republish its self-description to the broker.
   */
  fun publishResourcesToCoreContainer(resources: List<Resource>, retries: Int = 3, retryInterval: Long = 10000L) {
    if (coreContainerConfig.apiEndpoint.isNullOrBlank()) {
      throw RuntimeException("No Core Container API endpoint set")
    }

    val catalog = ResourceCatalogBuilder()
      ._offeredResource_(resources)
      .build()

    try {
      coreContainerAPI.post("resources/$catalogId/batch", catalog, String::class.java)
      log.info("Published DataResources catalog to core-container")
      resourceCache.clear()
      resources.forEach { resourceCache[it.id] = it }
      return
    } catch (e: CoreContainerApiException) {
      log.error("Failed to post resource to core-container, HTTP: {}", e.message)
    }

    if (retries <= 1) {
      log.error("No retries left, stopping container")
      exitProcess(1)
    } else {
      log.error("Retrying")
      Thread.sleep(retryInterval)
      publishResourcesToCoreContainer(resources, retries - 1, retryInterval)
    }
  }

  /**
   * Let the Core container known there are new resources available in the Data App.
   * The Core Container will republish its self-description to the broker.
   */
  fun unpublishResourceToCoreContainer(resourceId: String, retries: Int = 3, retryInterval: Long = 10000L) {
    if (coreContainerConfig.apiEndpoint.isNullOrBlank()) {
      throw RuntimeException("No Core Container API endpoint set")
    }
    try {
      coreContainerAPI.delete("resources/$catalogId/${CoreContainerAPI.uriEncode(resourceId)}")
      log.info("Unpublished DataResource to core-container")
      resourceCache.remove(URI(resourceId))
      return
    } catch (e: CoreContainerApiException) {
      log.error("Failed to delete resource to core-container, HTTP: {}", e.message)
    }
  }
}