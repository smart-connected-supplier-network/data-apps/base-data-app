package nl.tno.ids.base

import de.fraunhofer.iais.eis.Connector
import nl.tno.ids.base.configuration.IdsConfig
import org.slf4j.LoggerFactory
import org.springframework.core.ParameterizedTypeReference
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.net.URLEncoder
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap

/**
 * Federated Broker data class for describing trusted MetaData Brokers
 */
data class FederatedBroker(
  /** Access URL (including endpoint) of the Broker */
  val accessUrl: String,
  /** IDS Identifier for the Broker */
  val connectorId: String,
  /** Tags attached to the broker, can be used to internally filter federation to certain types of brokers */
  val tags: List<String> = emptyList()
)

/**
 * Type of federation used when multiple Brokers are configured
 */
enum class FederationType {
  /** Send a query to all brokers for each query */
  ALWAYS,
  /** Send a query to all brokers only when the primary broker did not yield a result */
  ON_EMPTY_RESULT,
  /** Only send queries to the primary broker and ignore federated brokers  */
  NEVER
}

/**
 * Broker Client component that Data Apps can use to query the Broker (and possible federated Brokers).
 */
@Component
@EnableScheduling
class BrokerClient(
  /** IDS Data App configuration for federation & caching settings */
  private val idsConfig: IdsConfig,
  /** Core Container API for retrieving the list of federated brokers & sending out the query  */
  private val coreContainerAPI: CoreContainerAPI
) {
  private val log = LoggerFactory.getLogger(BrokerClient::class.java)
  /** Cache of full connector metadata retrieved earlier with time in milliseconds */
  private var connectorsCache = ConcurrentHashMap<String, Pair<Long, Connector>>()
  /** Cache of specific filters that yielded a response earlier with time in milliseconds */
  private val filterQueryToConnectorIDsCache = ConcurrentHashMap<String, Pair<Long, List<String>>>()
  /** List of federated brokers */
  private var federatedBrokers: List<FederatedBroker> = listOf()

  /** Return query for getting specific Connector object from the broker based on connector IDS ID */
  private fun getConnectorQuery(connectorID: String) = """
        PREFIX ids: <https://w3id.org/idsa/core/>
        DESCRIBE *
        WHERE {
          GRAPH <$connectorID> {
            ?s ?o ?p.
          }
        }
      """.trimIndent()

  /** Return query for filtering Connector objects from the broker based on a SPARQL triple string addition */
  private fun connectorFilterQuery(filterString: String) = """
      PREFIX ids: <https://w3id.org/idsa/core/>
      DESCRIBE * WHERE {
        GRAPH ?g {
          ?s ?p ?o.
          $filterString
        }
      }
    """.trimIndent()

  /** Retrieve list of federated brokers from the Core Container */
  @Scheduled(
    initialDelay = 30000,
    fixedDelay = 86400000)
  fun retrieveFederatedBrokers() {
    if (idsConfig.retrieveFederatedBrokers) {
      try {
        federatedBrokers = coreContainerAPI
          .get("broker/federated-brokers", responseType = null, responseParameterType = object : ParameterizedTypeReference<List<FederatedBroker>>() {})
          ?.filter { federatedBroker ->
            idsConfig.federationTagScope.isEmpty() ||
                federatedBroker.tags.any { idsConfig.federationTagScope.contains(it) }
          }?.also {
            log.info("Retrieved ${it.size} federated brokers")
            log.debug("$it")
          } ?: emptyList()
      } catch (e: CoreContainerApiException) {
        log.warn("Could not retrieve federated brokers: ${e.message} (${e.statusCode})")
      }
    }
  }

  /** Query broker with specific SPARQL query, return a list of Connector objects */
  fun queryBroker(query: String, federationType: FederationType = idsConfig.defaultFederationType): List<Connector> {
    try {
      log.debug("Querying broker:\n$query")
      val mainBroker: CompletableFuture<List<Connector>> = CompletableFuture.supplyAsync { queryMessageExecutor(query) }

      val connectors = when (federationType) {
        FederationType.ALWAYS -> {
          log.debug("Federating query to ${federatedBrokers.size} brokers")
          val futures = federatedBrokers.map { federatedBroker ->
            CompletableFuture.supplyAsync {
              queryMessageExecutor(query, federatedBroker.accessUrl, federatedBroker.connectorId)
            }
          }

          (listOf(mainBroker) + futures).flatMap {
            it.get()
          }
        }
        FederationType.ON_EMPTY_RESULT -> {
          mainBroker.get().ifEmpty {
            log.debug("Result of primary broker is empty, federating query to ${federatedBrokers.size} brokers")
            federatedBrokers.map { federatedBroker ->
              CompletableFuture.supplyAsync {
                queryMessageExecutor(query, federatedBroker.accessUrl, federatedBroker.connectorId)
              }
            }.flatMap { it.get() }
          }
        }
        FederationType.NEVER -> {
          mainBroker.get()
        }
      }

      log.debug("Connectors: ${connectors.map { it.id }}")
      return connectors
    } catch (e: Exception) {
      log.error("Error in querying broker:", e)
    }
    return listOf()
  }

  /** Execute Broker query request via the Core Container management API */
  private fun queryMessageExecutor(query: String, accessUrl: String? = null, connectorId: String? = null): List<Connector> {
    val queryParameters = mutableMapOf<String, String>()
    accessUrl?.let { queryParameters["accessUrl"] = it }
    connectorId?.let { queryParameters["connectorId"] = it }
    val endpoint = if (queryParameters.isNotEmpty()) {
      "description/query?" + queryParameters.map { "${it.key}=${URLEncoder.encode(it.value, "UTF-8")}" }.joinToString(separator = "=")
    } else {
      "description/query"
    }
    return try {
      coreContainerAPI.post(endpoint, query, responseType = null, responseParameterType = object : ParameterizedTypeReference<List<Connector>>() {})
        ?: emptyList()
    } catch (e: CoreContainerApiException) {
      log.warn("Could not retrieve federated brokers: ${e.message} (${e.statusCode})")
      emptyList()
    }
  }

  /** Clear all caches */
  fun invalidateCaches() {
    log.info("Invalidating caches")
    log.debug("Invalidated keys: ${connectorsCache.keys()}")
    connectorsCache.clear()
    filterQueryToConnectorIDsCache.clear()
  }

  /**
   * Return Connector object (from cache, if present and valid and requested, else queried from broker) given
   * connector ID
   */
  fun getConnector(connectorID: String, useCache: Boolean = true): Connector? {
    if (useCache) {
      connectorsCache[connectorID]?.let { cachedEntry ->
        if (System.currentTimeMillis() - cachedEntry.first < idsConfig.cacheInvalidationPeriod) {
          log.debug("Returning ${cachedEntry.second.id} from cache")
          return cachedEntry.second
        } else {
          connectorsCache.remove(connectorID)
        }
      }
    }

    return queryBroker(getConnectorQuery(connectorID)).firstOrNull()?.also { connector ->
      connectorsCache[connectorID] = Pair(System.currentTimeMillis(), connector)
    }
  }

  /** Return list of Connector objects (from filterString cache ID references, if filter string is present in cache and
   * valid and requested, else queried from broker) given connector ID */
  fun queryConnectors(filterString: String, useCache: Boolean = true): List<Connector> {
    if (useCache) {
      filterQueryToConnectorIDsCache[filterString]?.let { filterCacheEntry ->
        if (System.currentTimeMillis() - filterCacheEntry.first < idsConfig.cacheInvalidationPeriod) {
          val connectors = connectorsCache
            .filter { it.key in filterCacheEntry.second }
            .map { connectorCacheEntry -> connectorCacheEntry.value.second }
          log.debug("Returning from cached query:\n$filterString")
          log.debug("Connectors: ${connectors.map { it.id }}")
          return connectors
        } else {
          filterQueryToConnectorIDsCache.remove(filterString)
        }
      }
    }

    return queryBroker(connectorFilterQuery(filterString)).also { connectors ->
      val timestamp = System.currentTimeMillis()
      filterQueryToConnectorIDsCache[filterString] =
        Pair(timestamp, connectors.map { connector -> connector.id.toString() })
      connectors.forEach { connector ->
        connectorsCache[connector.id.toString()] = Pair(timestamp, connector)
      }
    }
  }

  /** Query all connectors at the broker, cache the result and return the collection */
  fun updateConnectors(): Map<String, Connector> {
    return queryConnectors("", false).associateBy { connector ->
      connector.id.toString()
    }
  }
}
