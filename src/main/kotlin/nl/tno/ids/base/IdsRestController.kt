package nl.tno.ids.base

import de.fraunhofer.iais.eis.Message
import nl.tno.ids.base.multipart.MultiPartMessage
import org.apache.commons.io.IOUtils
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.Async
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.CompletableFuture

/**
 * Primary IDS Multipart controller that acts as primary ingress for IDS HTTP MIME Multipart messages
 */
@RestController
class IdsRestController(
  /** Message handler dispatcher for routing messages to the corresponding message handlers */
  private val messageHandlerDispatcher: MessageHandlerDispatcher,
  /** HTTP Helper for asynchronous IDSCP communication */
  private val httpHelper: HttpHelper,
  /** Broker Client for retrieving the access URL used for asynchronous IDSCP communication */
  private val brokerClient: BrokerClient,
  /** Response manager for maintaining a correct state of responses for asynchronous IDSCP communication */
  private val responseManager: ResponseManager,
  ) {
  private val log = LoggerFactory.getLogger(IdsRestController::class.java)

  /**
   * Data App HTTP endpoint handler, accepting forwarded messages from the core container
   * Routes the message to the correct Message handler based on the header.
   */
  @RequestMapping(value = ["/router"], method = [RequestMethod.POST], produces = [MediaType.MULTIPART_FORM_DATA_VALUE, "multipart/mixed"])
  @Async
  fun router(
    inputStream: InputStream,
    @RequestHeader httpHeaders: HttpHeaders
  ): CompletableFuture<ResponseEntity<*>> {
    return CompletableFuture.supplyAsync {
      val tmp = File.createTempFile("router", "http")
      IOUtils.copy(inputStream, tmp.outputStream())
      val multiPartMessage = try {
        MultiPartMessage.parse(tmp.inputStream(), httpHeaders.getFirst("Content-Type")).also {
          log.info("Received message: ${it.header.javaClass.canonicalName} of ${tmp.length()} bytes")
        }
      } catch (e: Exception) {
        log.warn("Received unreadable MultipartMessage: ${e.message} of ${tmp.length()} bytes", e)
        tmp.inputStream().use {
          log.info(String(it.readNBytes(1024)))
        }
        return@supplyAsync ResponseEntity.status(HttpStatus.BAD_REQUEST).build<Any>()
      } finally {
          tmp.delete()
      }
      // Check if this message is a response to another message we sent earlier.
      if (multiPartMessage.header.correlationMessage != null) {
        // Check if we need to trigger/complete any futures based on the response.
        val messageId = multiPartMessage.header.correlationMessage.toString()

        // Check if this message has futures waiting to be completed and if so, remove it from the list and execute it.
        val handled = responseManager
          .removeResponseHandler(messageId)
          ?.complete(Pair(multiPartMessage.header, multiPartMessage.payload?.inputStream()))
          ?: false
        if (!handled && httpHeaders.getFirst("Response-Type") == "async") {
          log.error("No completable future found for message: {}", messageId)
          return@supplyAsync ResponseEntity.notFound().build<Any>()
        }
        if (handled) {
          return@supplyAsync ResponseEntity.ok().build<Any>()
        }
      }
      httpHeaders.forEach { (key, value) -> multiPartMessage.httpHeaders[key] = value.first() }
      val response: ResponseEntity<*> = messageHandlerDispatcher.handleMessage(multiPartMessage).orElseGet {
        ResponseEntity.badRequest().build<Any>()
      }

      if (httpHeaders.getFirst("Response-Type") == "async") {
        // IDSCP messages are async. IDSCP response is sent back to the core container through a different channel.
        if (response.body == null) {
          return@supplyAsync ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        }
        try {
          val message = MultiPartMessage.parse(response.body as String)
          val receiver = message.header.recipientConnector[0].toString()
          val receiverEndpoint = brokerClient.getConnector(receiver)?.hasDefaultEndpoint?.accessURL?.toString() ?: receiver
          val status = httpHelper.toIDSCP(receiverEndpoint, message.header, message.payload?.asString())
          if (status > 201) {
            log.error("HTTP status {} in sending message to IDSCP receiver endpoint ({})", status, receiverEndpoint)
          }
        } catch (e: ClassCastException) {
          log.error("Could not send async response: ", e)
          return@supplyAsync ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        } catch (e: IOException) {
          log.error("Could not send async response: ", e)
          return@supplyAsync ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build<Any>()
        }
        return@supplyAsync ResponseEntity.status(response.statusCode).build<Any>()
      } else {
        // HTTP response is sent back to core-container.
        return@supplyAsync response
      }
    }
  }


  /**
   * Checks if any futures are waiting to be completed based on a response message.
   */
  private fun asyncResult(header: Message, payload: InputStream?): ResponseEntity<*> {
    val messageId = header.correlationMessage.toString()

    // Check if this message has futures waiting to be completed and if so, remove it from the list and execute it.
    responseManager
      .removeResponseHandler(messageId)
      ?.complete(Pair(header, payload))
      ?: run {
      log.error("No completable future found for message: {}", messageId)
    }
    return ResponseEntity.ok().build<Any>()
  }

  /**
   * Healthcheck available on /health, used by for instance Kubernetes health checks.
   */
  @RequestMapping(value = ["/health"], method = [RequestMethod.GET])
  fun healthCheck(): ResponseEntity<*> {
    return ResponseEntity.ok().build<Any>()
  }
}