package nl.tno.ids.base

import de.fraunhofer.iais.eis.Message
import nl.tno.ids.base.multipart.MultiPartMessage
import org.springframework.http.HttpHeaders
import kotlin.Throws
import java.io.IOException
import org.springframework.http.ResponseEntity
import java.io.InputStream

/**
 * MessageHandler interface for handling IDS messages. This interface should be implemented by a custom MessageHandler
 * and annotated with the @Component Spring Boot annotation. Reads payload body as InputStream.
 */
interface MessageHandler<I : Message> {
    @kotlin.jvm.Throws(IOException::class)
    fun handle(multiPartMessage: MultiPartMessage<I>): ResponseEntity<*> {
        val httpHeaders = HttpHeaders()
        multiPartMessage.httpHeaders.forEach { (key, value) ->
            httpHeaders[key] = value
        }
        return handle(multiPartMessage.header, multiPartMessage.payload?.inputStream(), httpHeaders)
    }

    @kotlin.jvm.Throws(IOException::class)
    fun handle(header: I, payload: InputStream?, httpHeaders: HttpHeaders): ResponseEntity<*> {
        throw NotImplementedError("Handle function of this (${this.javaClass.name}) message handler is not implemented")
    }
}

/**
 * MessageHandler interface for handling IDS messages. This interface should be implemented by a custom MessageHandler
 * and annotated with the @Component Spring Boot annotation. Reads payload body as String.
 */
interface StringMessageHandler<I : Message>: MessageHandler<I> {
    @Throws(IOException::class)
    fun handle(header: I, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*>

    @Throws(IOException::class)
    override fun handle(header: I, payload: InputStream?, httpHeaders: HttpHeaders): ResponseEntity<*> {
        return this.handle(header, payload?.let { String(it.readBytes()) }, httpHeaders)
    }
}