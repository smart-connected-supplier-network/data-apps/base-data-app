package nl.tno.ids.base

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.net.URI

/**
 * Helper class for response messages.
 */
@Component
class ResponseBuilder(private val idsConfig: IdsConfig) {
    /**
     * Helper method to create a RejectionMessageBuilder.
     */
    fun createRejectionMessage(message: Message, reason: RejectionReason?): RejectionMessageBuilder {
        return RejectionMessageBuilder()
            ._modelVersion_(message.modelVersion)
            ._issued_(DateUtil.now())
            ._securityToken_(
                DynamicAttributeTokenBuilder()
                    ._tokenFormat_(TokenFormat.JWT)
                    ._tokenValue_("DUMMY")
                    .build()
            )
            ._correlationMessage_(message.id)
            ._issuerConnector_(URI(idsConfig.connectorId))
            ._senderAgent_(message.recipientAgent.firstOrNull())
            ._recipientConnector_(message.issuerConnector)
            ._recipientAgent_(message.senderAgent)
            ._rejectionReason_(reason)
    }

    /**
     * Helper method to create a RejectionResponse.
     */
    fun createRejectionResponse(message: Message, reason: RejectionReason?, status: HttpStatus): ResponseEntity<*> {
        return ResponseEntity.status(status).body(
            MultiPartMessage(
                createRejectionMessage(message, reason).build()
            ).toString()
        )
    }
    /**
     * Helper method to create a RejectionResponse.
     */
    fun createMessageProcessedResponse(correlationMessage: Message): ResponseEntity<*> {
        return ResponseEntity.ok(
            MultiPartMessage(
                createMessageProcessedNotification(correlationMessage).build()
            ).toString()
        )
    }

    /**
     * Helper method to create a MessageProcessedNotificationMessageBuilder.
     */
    fun createMessageProcessedNotification(message: Message): MessageProcessedNotificationMessageBuilder {
        return MessageProcessedNotificationMessageBuilder()
            ._modelVersion_(message.modelVersion)
            ._issued_(DateUtil.now())
            ._securityToken_(
                DynamicAttributeTokenBuilder()
                    ._tokenFormat_(TokenFormat.JWT)
                    ._tokenValue_("DUMMY")
                    .build()
            )
            ._correlationMessage_(message.id)
            ._issuerConnector_(URI(idsConfig.connectorId))
            ._senderAgent_(message.recipientAgent.firstOrNull())
            ._recipientConnector_(message.issuerConnector)
            ._recipientAgent_(message.senderAgent)
    }

    /**
     * Helper method to create a ResultMessageBuilder.
     */
    fun createResultMessage(message: Message): ResultMessageBuilder {
        return ResultMessageBuilder()
            ._modelVersion_(idsConfig.modelVersion)
            ._issued_(DateUtil.now())
            ._securityToken_(
                DynamicAttributeTokenBuilder()
                    ._tokenFormat_(TokenFormat.JWT)
                    ._tokenValue_("DUMMY")
                    .build()
            )
            ._correlationMessage_(message.id)
            ._senderAgent_(message.recipientAgent.firstOrNull())
            ._issuerConnector_(URI(idsConfig.connectorId))
            ._recipientConnector_(message.issuerConnector)
            ._recipientAgent_(message.senderAgent)
    }
}