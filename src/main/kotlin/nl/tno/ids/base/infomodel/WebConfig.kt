package nl.tno.ids.base.infomodel

import de.fraunhofer.iais.eis.ModelClass
import nl.tno.ids.common.serialization.SerializationHelper
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.core.convert.converter.ConverterFactory
import org.springframework.format.FormatterRegistry
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

/**
 * Spring Web MCV configurer for allowing direct IDS concept parsing
 */
@Configuration
class WebConfig : WebMvcConfigurer {
    companion object {
        private val LOG = LoggerFactory.getLogger(WebConfig::class.java)
    }

    /**
     * Disable URL decoding to be able to parse that manually
     */
    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.urlPathHelper?.isUrlDecode = false
        super.configurePathMatch(configurer)
    }

    /**
     * CrossOrigin Resource Sharing configuration, enabled by default given the distributed nature and primary usage
     * of Data Apps within a security domain.
     */
    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
        super.addCorsMappings(registry)
    }

    /**
     * ConverterFactory for IDS Infomodel Concepts
     *
     * Adds @context field to the json if it does not exists, for when a subresource is copied (e.g.
     * a ContractOffer from a SelfDescription)
     */
    override fun addFormatters(registry: FormatterRegistry) {
        registry.addConverterFactory(object : ConverterFactory<String, ModelClass> {
            override fun <T : ModelClass?> getConverter(targetType: Class<T>): Converter<String, T> {
                return Converter {
                    val jsonld = if (!it.contains("@context")) {
                        JSONObject(it).put("@context", JSONObject()
                            .put("ids", "https://w3id.org/idsa/core/")
                            .put("idsc", "https://w3id.org/idsa/code/")
                        ).toString()
                    } else {
                        it
                    }
                    try {
                        SerializationHelper.getInstance().fromJsonLD(jsonld, targetType)
                    } catch (e: Exception) {
                        LOG.warn("Could not parse JSON-LD", e)
                        throw e
                    }
                }
            }
        })
    }
}