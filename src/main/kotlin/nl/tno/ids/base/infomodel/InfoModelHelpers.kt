package nl.tno.ids.base.infomodel

import de.fraunhofer.iais.eis.*
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.common.serialization.DateUtil
import nl.tno.ids.common.serialization.SerializationHelper
import java.net.URI

/**
 * Kotlin extension function to add capability of directly converting an IDS Infomodel class to JSON-LD
 */
fun ModelClass.toJsonLD(serializationHelper: SerializationHelper? = null): String {
    return (serializationHelper ?: SerializationHelper.getInstance()).toJsonLD(this)
}

/**
 * Kotlin extension function to convert an IDS Infomodel Message to a MultiPartMessage object
 */
fun <T : Message> T.toMultiPartMessage(payload: String? = null, contentType: String? = null, httpHeaders: Map<String, String>? = null): MultiPartMessage<*> {
    return MultiPartMessage(this, payload, httpHeaders?.toMutableMap() ?: mutableMapOf(), contentType)
}

/**
 * Kotlin extension function to set default IDS Message fields that are almost always the same value
 */
fun <T : Message> T.defaults(dat: String? = null): T {
    securityToken = DynamicAttributeTokenBuilder()
        ._tokenFormat_(TokenFormat.JWT)
        ._tokenValue_(dat ?: "DUMMY")
        .build()
    modelVersion = "4.1.0"
    issued = DateUtil.now()
    return this
}

/**
 * Kotlin extension function to set default values for an IDS response to an earlier request
 */
fun <T : ResponseMessage> T.isResponseTo(message: Message?, idsConfig: IdsConfig? = null): T {
    defaults()
    correlationMessage = message?.id ?: URI("urn:ids:unknown")
    issuerConnector = idsConfig?.connectorId?.let { URI(it)} ?: message?.recipientConnector?.firstOrNull() ?: URI("urn:ids:unknown")
    senderAgent = idsConfig?.participantId?.let { URI(it)} ?: message?.recipientAgent?.firstOrNull() ?: URI("urn:ids:unknown")
    recipientConnector = arrayListOf(message?.issuerConnector ?: URI("urn:ids:unknown"))
    recipientAgent = arrayListOf(message?.senderAgent ?: URI("urn:ids:unknown"))
    return this
}

/**
 * Kotlin extension function to set default values for an IDS response to an earlier request
 */
fun <T : NotificationMessage> T.isResponseTo(message: Message?, idsConfig: IdsConfig? = null): T {
    defaults()
    correlationMessage = message?.id ?: URI("urn:ids:unknown")
    issuerConnector = idsConfig?.connectorId?.let { URI(it)} ?: message?.recipientConnector?.firstOrNull() ?: URI("urn:ids:unknown")
    senderAgent = idsConfig?.participantId?.let { URI(it)} ?: message?.recipientAgent?.firstOrNull() ?: URI("urn:ids:unknown")
    recipientConnector = arrayListOf(message?.issuerConnector ?: URI("urn:ids:unknown"))
    recipientAgent = arrayListOf(message?.senderAgent ?: URI("urn:ids:unknown"))
    return this
}