package nl.tno.ids.base

import nl.tno.ids.base.configuration.CoreContainerConfig
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.*
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.stereotype.Component
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.UnknownHttpStatusCodeException
import org.springframework.web.util.UriUtils
import java.lang.IllegalArgumentException
import java.nio.charset.Charset

open class CoreContainerApiException(message: String, val statusCode: Int): Exception(message)
class CoreContainerApiAuthorizationException(message: String): CoreContainerApiException("Core Container API error: $message", 403)
class CoreContainerApiClientException(message: String, statusCode: Int): CoreContainerApiException("Core Container API Client error: $message ($statusCode)", statusCode)
class CoreContainerApiServerException(message: String, statusCode: Int): CoreContainerApiException("Core Container API Server error: $message ($statusCode)", statusCode)

/** Generic Core Container Management API that automatically injects the required API key */
@Component
class CoreContainerAPI(
  /** Core Container configuration containing the information to interact with the Management API */
  private val coreContainerConfig: CoreContainerConfig,
  /** Spring Rest Template */
  private val restTemplate: RestTemplate
) {
  /** Constructor to configure an interceptor for the Rest Template that injects the API key */
  init {
    coreContainerConfig.apiKey?.let { apiKey ->
      restTemplate.interceptors.add(ClientHttpRequestInterceptor { request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution ->
        if (request.uri.toString().startsWith(coreContainerConfig.apiEndpoint ?: "")) {
          request.headers["Authorization"] = "Bearer $apiKey"
        }
        execution.execute(request, body)
      })
    }
  }

  companion object {
    /** Helper function to encode Strings to be used as path variables */
    fun uriEncode(s: String) = UriUtils.encode(s, Charset.defaultCharset())
  }

  /** HTTP GET request */
  fun <T> get(apiEndpoint: String, responseType: Class<T>?,
              responseParameterType: ParameterizedTypeReference<T>? = null, headers: Map<String, String>? = null): T? {
    val entity = constructHttpHeaders(headers)?.let { httpHeaders ->
      HttpEntity("", httpHeaders)
    }
    return exchange(apiEndpoint, HttpMethod.GET, responseType, responseParameterType, entity)
  }

  /** HTTP POST request */
  fun <T> post(apiEndpoint: String, body: Any?, responseType: Class<T>?,
               responseParameterType: ParameterizedTypeReference<T>? = null, headers: Map<String, String>? = null): T? {
    return exchange(apiEndpoint, HttpMethod.POST, responseType, responseParameterType, body?.let { HttpEntity<Any>(body, constructHttpHeaders(headers)) })
  }

  /** HTTP PUT request */
  fun <T> put(apiEndpoint: String, body: Any?, responseType: Class<T>?,
              responseParameterType: ParameterizedTypeReference<T>? = null, headers: Map<String, String>? = null): T? {
    return exchange(apiEndpoint, HttpMethod.PUT, responseType, responseParameterType, body?.let { HttpEntity<Any>(body, constructHttpHeaders(headers)) })
  }

  /** HTTP DELETE request with response object */
  fun <T> delete(apiEndpoint: String, responseType: Class<T>?,
                 responseParameterType: ParameterizedTypeReference<T>? = null, headers: Map<String, String>? = null): T? {
    val entity = constructHttpHeaders(headers)?.let { httpHeaders ->
      HttpEntity("", httpHeaders)
    }
    return exchange(apiEndpoint, HttpMethod.DELETE, responseType, responseParameterType, entity)
  }

  /** HTTP DELETE request in fire-and-forget fashion */
  fun delete(apiEndpoint: String, headers: Map<String, String>? = null) {
    val entity = constructHttpHeaders(headers)?.let { httpHeaders ->
      HttpEntity("", httpHeaders)
    }
    exchange(apiEndpoint, HttpMethod.DELETE, String::class.java, null, entity)
  }

  /** HTTP Exchange method that actually invokes the HTTP requests */
  fun <T> exchange(apiEndpoint: String, method: HttpMethod, responseType: Class<T>?,
                   responseParameterType: ParameterizedTypeReference<T>? = null,
                   httpEntity: HttpEntity<*>? = null): T? {
    return try {
      val response = if (responseType != null) {
        restTemplate.exchange(
          "${coreContainerConfig.apiEndpoint?.dropLastWhile { it == '/' }}/${apiEndpoint.dropWhile { it == '/' }}",
          method,
          httpEntity,
          responseType
        )
      } else if (responseParameterType != null) {
        restTemplate.exchange(
          "${coreContainerConfig.apiEndpoint}/$apiEndpoint",
          method,
          httpEntity,
          responseParameterType
        )
      } else {
        throw IllegalArgumentException("Either responseType or responseParameterType must be provided")
      }
      response.body
    } catch (e: HttpClientErrorException) {
      if (e.statusCode == HttpStatus.UNAUTHORIZED || e.statusCode == HttpStatus.FORBIDDEN) {
        throw CoreContainerApiAuthorizationException("Incorrect API key or insufficient rights")
      }
      throw CoreContainerApiClientException("Incorrect API call (${e.statusText})", e.rawStatusCode)
    } catch (e: HttpServerErrorException) {
      throw CoreContainerApiServerException("Core Container API error (${e.statusText})", e.rawStatusCode)
    } catch (e: UnknownHttpStatusCodeException) {
      throw CoreContainerApiException("Unknown Core Container API error: (${e.statusText})", e.rawStatusCode)
    }
  }

  /** Helper function to construct Spring HttpHeaders from a generic Kotlin Map */
  private fun constructHttpHeaders(headers: Map<String, String>?): HttpHeaders? {
    return headers?.let {
      HttpHeaders().apply {
        it.forEach { (name, value) ->
          this[name] = value
        }
      }
    }
  }
}