package nl.tno.ids.base

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import nl.tno.ids.common.serialization.SerializationHelper
import org.apache.http.client.config.RequestConfig
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import java.util.concurrent.Executor

/**
 * Generic Beans providing common functionality easily to implementations
 */
@Configuration
@EnableAsync
class SpringConfiguration : AsyncConfigurer {
  /**
   * This http client will be auto-wired in other classes.
   */
  @Bean
  fun httpClient(): CloseableHttpClient {
    val timeout = 180
    val config: RequestConfig = RequestConfig.custom()
      .setConnectTimeout(timeout * 1000)
      .setConnectionRequestTimeout(timeout * 1000)
      .setSocketTimeout(timeout * 1000).build()
    return HttpClientBuilder.create().setDefaultRequestConfig(config).build()
  }

  /** IDS Serialization Helper bean */
  @Bean
  fun getJsonLdSerializer(): SerializationHelper {
    return SerializationHelper.getInstance()
  }

  /** Jackson Object mapper including the Kotlin Module for Kotlin Data Class support */
  @Bean
  fun objectMapper(): ObjectMapper {
    return ObjectMapper().registerModule(KotlinModule.Builder().build())
  }

  /** Primary Spring executor for asynchronous handling of requests */
  override fun getAsyncExecutor(): Executor {
    return ThreadPoolTaskExecutor().apply {
      corePoolSize = 10
      keepAliveSeconds = 60
      initialize()
    }
  }
}
