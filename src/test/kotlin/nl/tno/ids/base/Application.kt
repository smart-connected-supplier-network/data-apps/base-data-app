package nl.tno.ids.base

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

/**
 * Spring Boot test context
 */
@SpringBootApplication
@ConfigurationPropertiesScan(basePackages = ["nl.tno.ids", ""])
class Application {
  fun main(args: Array<String>) {
    runApplication<Application>(*args)
  }
}
