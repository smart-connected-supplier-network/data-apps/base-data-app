package nl.tno.ids.base

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

/** Simple Spring Boot test to verify the Spring context loads successfully */
@SpringBootTest
class ApplicationStartup {
  @Test
  fun contextLoads() {
  }
}
