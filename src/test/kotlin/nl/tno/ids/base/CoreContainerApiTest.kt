package nl.tno.ids.base

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.VerificationException
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.stubbing.StubMapping
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.base.infomodel.toJsonLD
import org.junit.jupiter.api.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.ParameterizedTypeReference
import org.springframework.test.context.TestPropertySource
import java.net.URI
import kotlin.math.floor

/**
 * Core Container Management API test, including Resource state management
 */
@SpringBootTest
@TestPropertySource(properties = ["ids.validateResources=true", "ids.validateResourcesInterval=1500"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class CoreContainerApiTest {

  @Autowired
  private lateinit var coreContainerAPI: CoreContainerAPI

  @Autowired
  private lateinit var resourcePublisher: ResourcePublisher

  @Test
  @Order(0)
  fun testPublishingUnpublishing() {
    coreContainerAPI.get("resources", null, object : ParameterizedTypeReference<List<ResourceCatalog>>() {})

    Assertions.assertEquals(1, resourcePublisher.getResources()?.offeredResource?.size)

    resourcePublisher.publishResourceToCoreContainer(createResource())
    resourcePublisher.publishResourcesToCoreContainer(listOf(createResource(), createResource()))
    resourcePublisher.unpublishResourceToCoreContainer(resourcePublisher.resourceCache.values.first().id.toString())

    wireMock.verify(1, WireMock.postRequestedFor(WireMock.urlEqualTo("/resources/urn%253Aids%253Aconnectors%253ATest%253Adata-app")))
    wireMock.verify(1, WireMock.postRequestedFor(WireMock.urlEqualTo("/resources/urn%253Aids%253Aconnectors%253ATest%253Adata-app/batch")))
    wireMock.verify(1, WireMock.deleteRequestedFor(WireMock.urlPathMatching("/resources/urn%253Aids%253Aconnectors%253ATest%253Adata-app/(.*)")))
  }

  @Test
  @Order(1)
  fun testResourceValidationInconsistent() {
    wireMock.resetRequests()
    verifyMock(2000) {
      wireMock.verify(WireMock.moreThanOrExactly(1), WireMock.postRequestedFor(WireMock.urlPathMatching("/resources/(.*)/batch")))
    }

    wireMock.removeStub(catalogStub)
  }

  @Test
  @Order(2)
  fun testResourceValidationConsistent() {
    wireMock.stubFor(
      WireMock.get(WireMock.urlPathMatching("/resources/(.*)"))
        .willReturn(
          WireMock.aResponse()
            .withStatus(200)
            .withHeader("Content-Type", "application/ld+json")
            .withBody(
              ResourceCatalogBuilder()
                ._offeredResource_(resourcePublisher.resourceCache.values.toList())
                .build()
                .toJsonLD()
            )
        )
    )
    wireMock.resetRequests()
    verifyMock(2000) {
      wireMock.verify(0, WireMock.postRequestedFor(WireMock.urlPathMatching("/resources/(.*)/batch")))
    }
  }

  private fun verifyMock(timeout: Long, function: () -> Unit) {
    for (i in 0..floor(timeout/100.0).toInt()) {
      try {
        function.invoke()
        return
      } catch (e: VerificationException) {
        Thread.sleep(100)
      }
    }
    function.invoke()
  }

  companion object {
    private lateinit var wireMock: WireMockServer
    private const val wireMockPort = 32145

    fun createResource(): Resource = ResourceBuilder()
      ._title_(TypedLiteral("Resource Title", "en"))
      ._description_(TypedLiteral("Resource description", "en"))
      ._sovereign_(URI("urn:ids:AgentA"))
      ._resourceEndpoint_(
        ConnectorEndpointBuilder()
          ._accessURL_(URI("http://localhost"))
          ._path_("/resourceTest")
          .build()
      )
      .build()

    private var resources = listOf(createResource())
    private lateinit var catalogStub: StubMapping

    @JvmStatic
    @BeforeAll
    fun setupWireMockDaps() {
      wireMock = WireMockServer(wireMockPort).apply { start() }
      wireMock.stubFor(
        WireMock.get(WireMock.urlPathMatching("/resources"))
          .willReturn(
            WireMock.aResponse()
              .withStatus(200)
              .withHeader("Content-Type", "application/ld+json")
              .withBody(
                "[${ResourceCatalogBuilder()
                  ._offeredResource_(resources)
                  .build()
                  .toJsonLD()}]"
              )
          )
      )
      catalogStub = wireMock.stubFor(
        WireMock.get(WireMock.urlPathMatching("/resources/(.*)"))
          .willReturn(
            WireMock.aResponse()
              .withStatus(200)
              .withHeader("Content-Type", "application/ld+json")
              .withBody(
                ResourceCatalogBuilder()
                  ._offeredResource_(resources)
                  .build()
                  .toJsonLD()
              )
          )
      )
      wireMock.stubFor(
        WireMock.post(WireMock.urlPathMatching("/resources/(.*)"))
          .willReturn(
            WireMock.aResponse()
              .withStatus(200)
          )
      )
      wireMock.stubFor(
        WireMock.post(WireMock.urlPathMatching("/resources/(.*)/batch"))
          .willReturn(
            WireMock.aResponse()
              .withStatus(200)
          )
      )
      wireMock.stubFor(
        WireMock.delete(WireMock.urlPathMatching("/resources/(.*)/(.*)"))
          .willReturn(
            WireMock.aResponse()
              .withStatus(200)
          )
      )
    }
  }

}