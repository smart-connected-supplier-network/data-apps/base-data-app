package nl.tno.ids.base

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder
import com.github.tomakehurst.wiremock.client.WireMock.aResponse
import com.github.tomakehurst.wiremock.client.WireMock.post
import com.github.tomakehurst.wiremock.common.FileSource
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import com.github.tomakehurst.wiremock.extension.Parameters
import com.github.tomakehurst.wiremock.extension.ResponseDefinitionTransformer
import com.github.tomakehurst.wiremock.http.Request
import com.github.tomakehurst.wiremock.http.ResponseDefinition
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.base.infomodel.toJsonLD
import nl.tno.ids.base.infomodel.toMultiPartMessage
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.InputStreamEntity
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.test.context.TestPropertySource
import java.io.File
import java.io.InputStream
import java.net.URI
import java.util.concurrent.CompletableFuture

@Component
class InvokeOperationMessageHandler : StringMessageHandler<InvokeOperationMessage> {
  override fun handle(header: InvokeOperationMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
    return ResponseEntity.ok("InvokeOperationMessageHandler")
  }
}

@Component
class QueryMessageHandler : StringMessageHandler<QueryMessage> {
  override fun handle(header: QueryMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
    return ResponseEntity.ok("QueryMessageHandler")
  }
}

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ["spring.config.location = classpath:application.yaml"])
class MessageHandlerTests {
  @LocalServerPort
  lateinit var port: String

  @Autowired
  lateinit var brokerClient: BrokerClient

  @Autowired
  lateinit var responseManager: ResponseManager

  companion object {
    @JvmField
    @RegisterExtension
    val wiremockServer: WireMockExtension = WireMockExtension.newInstance().options(
      wireMockConfig().port(32461).extensions(CacheTestResponseBuilderTransformer())
    ).build()
  }

  @Test
  fun testInvokeOperationMessage() {
    val invokeOperationMessage = InvokeOperationMessageBuilder()
      ._operationReference_(URI("http://connector.local"))
      ._issuerConnector_(URI("urn:ids:testReceiver"))
      ._senderAgent_(URI("urn:ids:testSender"))
      ._recipientConnector_(URI("urn:ids:testReceiver"))
      ._recipientAgent_(URI("urn:ids:testReceiver"))
      ._securityToken_(DynamicAttributeTokenBuilder()._tokenFormat_(TokenFormat.SAML_1_1)._tokenValue_("test").build())
      .build()
      .defaults()

    val httpPost = HttpPost("http://localhost:$port/router")
    val file = File.createTempFile("base-data-app-", ".http")
    try {
      invokeOperationMessage.toMultiPartMessage().writeToOutputStream(file.outputStream(), true)
      httpPost.entity = InputStreamEntity(file.inputStream())

      HttpClients.createDefault().execute(httpPost).use {
        assertEquals(200, it.statusLine.statusCode)
        assertEquals("InvokeOperationMessageHandler", String(it.entity.content.readAllBytes()))
      }
    } finally {
      file.delete()
    }
  }

  @Test
  fun testParticipant() {
    ParticipantBuilder(URI("https://ids.tno.nl/participants/TNO"))
      ._title_(
        arrayListOf(
          TypedLiteral("Nederlandse Organisatie voor toegepast-natuurwetenschappelijk onderzoek", "nl"),
          TypedLiteral("Netherlands Organisation for Applied Scientific Research", "en")
        )
      )
      ._memberParticipant_(
        arrayListOf(
          ParticipantBuilder(URI("https://ids.tno.nl/participants/TNO/MCS"))
            ._title_(TypedLiteral("Monitoring & Control Services", "en"))
            .build(),
          ParticipantBuilder(URI("https://ids.tno.nl/participants/TNO/DS"))
            ._title_(TypedLiteral("Data Science", "en"))
            .build()
        )
      )
      ._memberPerson_(
        arrayListOf(
          PersonBuilder()
            ._givenName_("Maarten")
            ._familyName_("Kollenstart")
            ._emailAddress_("maarten.kollenstart@tno.nl")
            ._phoneNumber_("0611428437")
            .build()
        )
      )
      ._corporateHomepage_(URI("https://tno.nl"))
      ._corporateEmailAddress_("contact@tno.nl")
      ._primarySite_(SiteBuilder()._siteAddress_("Anna van Buerenplein 1, 2595 DA Den Haag").build())
      .build()
      .toJsonLD()
  }


  @Test
  fun testQueryMessage() {
    val queryMessage = QueryMessageBuilder()
      ._issuerConnector_(URI("urn:ids:testReceiver"))
      ._senderAgent_(URI("urn:ids:testSender"))
      ._recipientConnector_(URI("urn:ids:testReceiver"))
      ._recipientAgent_(URI("urn:ids:testReceiver"))
      ._recipientScope_(QueryTarget.BROKER)
      .build()
      .defaults()

    val httpPost = HttpPost("http://localhost:$port/router")
    httpPost.entity = StringEntity(queryMessage.toMultiPartMessage().toString())
    HttpClients.createDefault().execute(httpPost).use {
      assertEquals(200, it.statusLine.statusCode)
      assertEquals("QueryMessageHandler", String(it.entity.content.readAllBytes()))
    }
  }

  @Test
  fun responseManagerTest() {
    val invokeOperationMessage = InvokeOperationMessageBuilder()
      ._operationReference_(URI("https://ids.tno.nl/base/responseManagerTest"))
      ._issuerConnector_(URI("urn:ids:testReceiver"))
      ._senderAgent_(URI("urn:ids:testSender"))
      ._recipientConnector_(URI("urn:ids:testReceiver"))
      ._recipientAgent_(URI("urn:ids:testReceiver"))
      .build()
      .defaults()

    val future = CompletableFuture<Pair<Message, InputStream?>>()
    responseManager.registerResponseHandler(invokeOperationMessage.id.toString(), future)

    future.thenApply { (first, _) ->
      assert(first !is RejectionMessage)
    }


    val httpPost = HttpPost("http://localhost:$port/router")
    httpPost.entity = StringEntity(invokeOperationMessage.toMultiPartMessage().toString())
    HttpClients.createDefault().execute(httpPost).use {
      assertEquals(200, it.statusLine.statusCode)
    }
  }

  @Test
  fun cacheVerificationTest() {
    val resourceID1 = "urn:ids:TNO:resource1"
    val resourceID2 = "urn:ids:TNO:resource2"
    wiremockServer.stubFor(
      post("/https_out")
        .willReturn(
          aResponse().withHeader("Content-Type", "multipart/form-data").withBody("malformed-body")
            .withTransformers("connector-transformer")
        )
    )

    val sparqlQueryAddition = { sovereign: String -> "?c a ids:TrustedConnector. ?c ids:resourceCatalog ?cat. ?cat ids:offeredResource ?r. ?r ids:sovereign <$sovereign>"}
    brokerClient.queryConnectors(sparqlQueryAddition(resourceID1))
    assertNotNull(brokerClient.queryConnectors(sparqlQueryAddition(resourceID1)))
    assertNotNull(brokerClient.queryConnectors(sparqlQueryAddition(resourceID2)))
    assertNotNull(brokerClient.queryConnectors(sparqlQueryAddition(resourceID1)))

  }
}

class CacheTestResponseBuilderTransformer : ResponseDefinitionTransformer() {
  override fun getName(): String {
    return "connector-transformer"
  }

  override fun applyGlobally(): Boolean {
    return false
  }

  override fun transform(
    request: Request?, responseDefinition: ResponseDefinition?, files: FileSource?, parameters: Parameters?
  ): ResponseDefinition {
    val connectorIDSID1 = "urn:ids:connector1"
    val connectorIDSID2 = "urn:ids:connector2"
    if (request?.bodyAsString != null) {
      val queryMessage = MultiPartMessage.parse(request.bodyAsString)
      val sparqlQueryWithoutPrefix = queryMessage.payload?.asString()?.split("DESCRIBE")?.get(1)
      val regex = Regex("(?<=<)(.*?)(?=>)")
      val urnID: String = regex.find(sparqlQueryWithoutPrefix.toString())?.value
        ?: throw Exception("Resource URN ID could not be parsed properly, SPARQL query may have been formed improperly.")
      val connectorData = when (urnID) {
        "urn:ids:TNO:resource1" -> buildBrokerResponse(connectorIDSID1, urnID)
        "urn:ids:TNO:resource2" -> buildBrokerResponse(connectorIDSID2, urnID)
        else -> throw Exception("Wrong Resource URN ID passed.")
      }
      return ResponseDefinitionBuilder().withHeader("Content-Type", "multipart/form-data").withStatus(200)
        .withBody(connectorData).build()
    } else {
      throw Exception("Formation of connector description is empty or invalid.")
    }
  }

  private fun buildBrokerResponse(connectorID: String, resourceID: String): String {
    var connectorStr =
      TrustedConnectorBuilder(URI(connectorID))
        ._version_("4.1.1")
        ._title_(TypedLiteral("Connector"))
        ._maintainer_(URI("urn:ids:TNO"))
        ._curator_(URI("urn:ids:TNO"))
        ._outboundModelVersion_("4.1.1")
        ._hasDefaultEndpoint_(
          ConnectorEndpointBuilder()._accessURL_(URI("http://localhost:32222")).build()
        )
        ._resourceCatalog_(
          ResourceCatalogBuilder()._offeredResource_(
            ResourceBuilder()._resourceEndpoint_(
              ConnectorEndpointBuilder(URI(resourceID))._accessURL_(URI("http://localhost:32224")).build()
            ).build()
          ).build()
        )
        ._securityProfile_(SecurityProfile.TRUST_SECURITY_PROFILE).build().toJsonLD()
    connectorStr = "[$connectorStr]"

    val brokerResponseHeader: ResultMessage =
      ResultMessageBuilder()
        ._issuerConnector_(URI("urn:ids:broker-dummy"))
        ._senderAgent_(URI("urn:ids:tno"))
        ._recipientConnector_(URI("urn:ids:connector-dummy"))
        ._recipientAgent_(URI("urn:ids:connector-dummy"))
        ._correlationMessage_(URI("urn:ids:dummy"))
        .build()
        .defaults()

    val connectorMultiPart = MultiPartMessage(header = brokerResponseHeader, payload = connectorStr)
    return connectorMultiPart.toString()
  }

}
